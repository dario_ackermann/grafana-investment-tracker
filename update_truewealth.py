import requests
import pyotp
import mysql.connector
from datetime import datetime
from secrets_app import *
import secrets
import string

alphabet = string.ascii_uppercase + string.digits

def log(msg):
    print("[{}] {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), msg))

s = requests.Session()

log("Connecting to MySQL...")

cnx = mysql.connector.connect(
    database=DB_DATABASE,
    password=DB_PASSWORD,
    username=DB_USERNAME,
    host=DB_HOST,
    port=DB_PORT,
)
cnx.autocommit = True
cursor = cnx.cursor(prepared=True)

s.headers = {
    "accept": "*/*",
    "accept-language": "de-DE,de;q=0.6",
    "sec-ch-ua": "\"Brave\";v=\"111\", \"Not(A:Brand\";v=\"8\", \"Chromium\";v=\"111\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"macOS\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-origin",
    "sec-gpc": "1",
    "x-client-version": "v217.6.0",
    "x-correlation-id": "ur86oq6",
    "x-requested-with": "XMLHttpRequest",
    "Referrer-Policy": "same-origin"
}

def gen_xsrf_token():
    s.headers["x-xsrf-token"] = ''.join(secrets.choice(alphabet) for i in range(32))
    s.cookies.set("__Host-XSRF-Token", s.headers["x-xsrf-token"])


log("Logging in to TrueWealth as {}...".format(TW_USERNAME))


gen_xsrf_token()
data = '{"loginId":"'+TW_USERNAME+'","password":"'+TW_PASSWORD+'"}'
response = s.post('https://app.truewealth.ch/api/auth/login', data=data)
if response.status_code == 422:
    token = pyotp.TOTP(TW_TFA).now()
    log("Using 2FA Token...")
    data = '{"loginId":"'+TW_USERNAME+'","password":"'+TW_PASSWORD+'","token":"'+token+'"}'
    response = s.post('https://app.truewealth.ch/api/auth/login', data=data)
elif response.status_code != 204:
    log("Login failed")
    exit()

assert response.status_code == 204
log("Logged in.")

log("Loading available portfolios")
gen_xsrf_token()
portfolios = s.get("https://app.truewealth.ch/api/portfolios")
assert portfolios.status_code == 200
portfolio_id = portfolios.json()[0]["id"]
log("Loading Evolution Data for Portfolio {}".format(portfolio_id))

gen_xsrf_token()
evolution = s.get("https://app.truewealth.ch/api/portfolios/{}/evolution".format(portfolio_id))
assert evolution.status_code == 200
evolution = evolution.json()

for day_data in evolution["performance"]:

    #log("Processing data for {}     ".format(day_data["date"]))

    stmt = "INSERT INTO truewealth_evolution(date, start, end, inflows, outflows, grossReturn, netReturn, c_inflows, c_grossReturn, c_netReturn) " \
           "VALUES(?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE start = ?, end = ?, inflows = ?, outflows = ?, grossReturn = ?, netReturn = ?, c_inflows = ?, c_grossReturn = ?, c_netReturn = ?"

    cursor.execute(stmt, (day_data["date"], day_data["vStart"], day_data["vEnd"], day_data["inflows"], day_data["outflows"], day_data["grossReturn"], day_data["netReturn"],
                          day_data["cumulative"]["inflows"], day_data["cumulative"]["grossReturn"], day_data["cumulative"]["netReturn"], day_data["vStart"], day_data["vEnd"], day_data["inflows"], day_data["outflows"], day_data["grossReturn"], day_data["netReturn"],
                          day_data["cumulative"]["inflows"], day_data["cumulative"]["grossReturn"], day_data["cumulative"]["netReturn"], ))

    cnx.commit()
log(f"Processed {len(evolution['performance'])} records.")

log("Updating Updated-Flag")
cursor.close()
cursor = cnx.cursor()
cursor.execute("UPDATE updated SET dt = NOW() WHERE `name` = 'truewealth'", ())
cnx.commit()
cursor.close()
cnx.close()
log("Done")
