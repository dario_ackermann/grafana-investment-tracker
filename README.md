# Selma & TW Investment Tracker

![alt text](dashboard.png "Dashboard")

## Motivation

I wanted to better track my investments and to be able to see and easily compare portfolio performances.  
For this, I created these tools to collect and display this data.  
If you have any suggestions or ideas, merge requests are very welcome.

## Requirements

### ⚠️ WARNING ⚠️

In case you intend to use 2FA, make sure your end device time is synced and correct (!), 
otherwise you will calculate wrong 2FA-Codes which will block you out from the services!!!

### Dashboard:

You need to be able to install a Grafana Dashboard (>= v7) and need to know how to import JSON Files

### Accounts:

Usage of **token-based** 2FA is **recommended** (but not mandatory), for both, TrueWealth and Selma accounts.

### Software:

 `python3.8` or higher is needed. The modules are installed in the next step.
 
## Basic Setup

1. Run `pip3 install -r requirements.txt` to install requirements
2. Create a mysql database (e.g. MariaDB) and import `wealth.sql` to create the tables (e.g. with PHPMyAdmin this can be done easily)
3. Also make sure this database has a user that has full read/write access to this database
4. Copy `secrets_app.sample.py` to `secrets_app.py` and edit all fields accordingly.  
5. Create a data source with your Database in Grafana, import `grafana_investing.json` as new dashboard, adjust data source and variables.

## Loading Data

Simply run the update scripts with `python3` periodically (see section Cronjob for an example usage)
Old data will be imported automatically, there's nothing you need to do

## Cronjob

An example cronjob (`crontab -e`)
 can look like this. It will update the data every 4 hours:
```bash
0 */4 * * * python3 /path/to/grafana-investment-tracker/update_truewealth.py >> path/to/logging/truewealth.log
0 */4 * * * python3 /path/to/grafana-investment-tracker/update_selma.py >> path/to/logging/selma.log
```
