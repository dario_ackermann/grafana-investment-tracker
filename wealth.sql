-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 30. Dez 2020 um 16:26
-- Server-Version: 5.7.32-0ubuntu0.18.04.1
-- PHP-Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `wealth`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `selma_evolution`
--

CREATE TABLE `selma_evolution` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `equity` double NOT NULL,
  `return` double NOT NULL,
  `return_tw` double NOT NULL,
  `return_mw` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `truewealth_evolution`
--

CREATE TABLE `truewealth_evolution` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start` double NOT NULL,
  `end` double NOT NULL,
  `inflows` double NOT NULL,
  `outflows` double NOT NULL,
  `grossReturn` double NOT NULL,
  `netReturn` double NOT NULL,
  `c_inflows` double NOT NULL,
  `c_grossReturn` double NOT NULL,
  `c_netReturn` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `updated`
--

CREATE TABLE `updated` (
  `name` varchar(255) NOT NULL,
  `dt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `updated`
--

INSERT INTO `updated` (`name`, `dt`) VALUES
('selma', '2020-12-30 15:14:49'),
('truewealth', '2020-12-30 15:15:03');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `selma_evolution`
--
ALTER TABLE `selma_evolution`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `date` (`date`);

--
-- Indizes für die Tabelle `truewealth_evolution`
--
ALTER TABLE `truewealth_evolution`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `date` (`date`);

--
-- Indizes für die Tabelle `updated`
--
ALTER TABLE `updated`
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `selma_evolution`
--
ALTER TABLE `selma_evolution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;
--
-- AUTO_INCREMENT für Tabelle `truewealth_evolution`
--
ALTER TABLE `truewealth_evolution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
