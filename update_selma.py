import mechanize
import pyotp
import mysql.connector
import json
import dateparser
import math
from datetime import datetime

from secrets_app import *

def log(msg):
    print("[{}] {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), msg))


log("Connecting MySQL")


mydb = mysql.connector.connect(
    host=DB_HOST,
    user=DB_USERNAME,
    password=DB_PASSWORD,
    port=DB_PORT,
    database=DB_DATABASE
)
cursor = mydb.cursor()


log("Starting browser")


br = mechanize.Browser()
br.set_handle_robots(False)
br.set_debug_redirects(True)
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
response = br.open("https://app.selma.com/de/users/sign_in")

log("Logging in with E-Mail and Password...")

br.select_form(nr=0)
br.form["user[email]"] = SELMA_EMAIL
br.form["user[password]"] = SELMA_PASSWORD
br.submit()

if br.forms():
    if len(SELMA_TFA) < 10:
        log("ABORTING! Selma requests Twofactor-Authentication but no key was specified")
        raise Exception("Selma is requesting 2FA but no 2FA-Key was specified in secrets.py - aborting")
    log("Performing 2FA....")
    br.select_form(nr=0)
    br.form["code"] = pyotp.TOTP(SELMA_TFA).now()
    br.submit()

log("Get Account Information...")
response = br.open("https://app.selma.com/api/accounts")
assert response.code == 200
data = response.read()


day_data = json.loads(data)

log("Get Account Transactions...")
response = br.open("https://app.selma.com/api/account_transactions")
assert response.code == 200
data = response.read()
transactions = json.loads(data)

log("Get Account Statuses...")
response = br.open("https://app.selma.com/api/account_statuses")
assert response.code == 200
data = response.read()
statuses = json.loads(data)


log("Calculating historic performancees...")

balances = {}
addals = {}
before = 0


for transaction in reversed(transactions):
    if transaction["transaction_type"] == "cash_transfer":

        balances[int(dateparser.parse(transaction["value_date"]).timestamp())] = {
            "date": transaction["value_date"],
            "c_inflows": before + float(transaction["amount_in_instrument_currency"])
        }

        before += float(transaction["amount_in_instrument_currency"])
        addals[transaction["value_date"]] = addals.get(transaction["value_date"], 0) + float(transaction["amount_in_instrument_currency"])


def find_c_inflows_for_day(date):
    ts = dateparser.parse(date).timestamp()
    last = {}
    for balance_ts in balances:
        balance = balances[balance_ts]
        if balance_ts > ts:
            return last.get("c_inflows", 0)
        last = balance
    return last.get("c_inflows", 0)

performances = {}
daily_tw_returns = {}
last_equity = 0

for status in statuses:
    te = float(status["total_equity"])
    cinflows = find_c_inflows_for_day(status["date"])

    if cinflows > 0:
        if len(daily_tw_returns) == 0:
            daily_tw_returns[status["date"]] = 1
        else:
            last_equity += addals.get(status["date"], 0)
            daily_tw_returns[status["date"]] = te / last_equity

    if cinflows == 0:
        continue

    performances[status["date"]] = {
        "total_return": {"percentage": 100*((te / cinflows) - 1)},
        "time_weighted_return": math.prod(daily_tw_returns.values()) - 1,
        "total_equity": te,
        "date": status["date"]
    }

    last_cinflow = cinflows
    last_equity = te

sql = " INSERT INTO selma_evolution(date, equity, `return`, return_tw, return_mw) " \
      "VALUES(%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE equity = %s, `return` = %s, return_tw = %s, return_mw = %s"

for date in performances:

    performance = performances[date]
    log("Inserting historic values for {}".format(date))
    # print(performance)
    inserts = (performance["date"],
               performance["total_equity"],
               str(float(performance["total_return"]["percentage"]) / 100),
               performance["time_weighted_return"],
               1,
               performance["total_equity"],
               str(float(performance["total_return"]["percentage"]) / 100),
               performance["time_weighted_return"],
               1, )

    cursor.execute(sql, inserts)
    mydb.commit()
    log("{} record(s) updated.".format(cursor.rowcount))


log("Inserting yesterdays' value")

sql = " INSERT INTO selma_evolution(date, equity, `return`, return_tw, return_mw) " \
      "VALUES(%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE equity = %s, `return` = %s, return_tw = %s, return_mw = %s"
inserts = (day_data[0]["date"],
           day_data[0]["total_equity"],
           str(float(day_data[0]["total_return"]["percentage"]) / 100),
           day_data[0]["time_weighted_return"],
           day_data[0]["money_weighted_return"],
           day_data[0]["total_equity"],
           str(float(day_data[0]["total_return"]["percentage"]) / 100),
           day_data[0]["time_weighted_return"],
           day_data[0]["money_weighted_return"])

cursor.execute(sql, inserts)
mydb.commit()
log(f"{cursor.rowcount} record(s) updated.")


log("Updating Updated-Flag")
cursor.close()
cursor = mydb.cursor()
cursor.execute("UPDATE updated SET dt = NOW() WHERE `name` = 'selma'", ())
mydb.commit()
cursor.close()
mydb.close()

log("Done.")
